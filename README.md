<!---
MG08/MG08 is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->

[![Social banner for Daru](https://gitlab.com/Darukeru/Darukeru/-/raw/main/assets/banner_gitlab_profile_opt.gif)](https://smolgaming.com)

# Hi, I'm Daru :wave:
- ✌ Hey, I’m @Darukeru, and I do videogame fan-translations for fun
- 👀 I’m a data scientist and software engineer
- 🔥 In my free time I love romhacking old console videogames
- 💞️ Love RPGs, Horror and Rock
- 📫 I'm skilled in web, desktop and android development, any ideas you want to make real?

For enquiries, reach out @ [daru@smolgaming.com](mailto:daru@smolgaming.com) or over on [Twitter](https://twitter.com/Darukeru).

# See yaaaa'!
<hr>

[My Ko-fi Page](https://ko-fi.com/J3J37YPRE)  
If you enjoy my work, please donate.  


<a href='https://ko-fi.com/J3J37YPRE' target='_blank'><img height='72' style='border:0px;height:72px;' src='https://cdn.ko-fi.com/cdn/kofi1.png?v=3' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a>